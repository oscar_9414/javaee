<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>Insert title here</title>
	</head>
	<body>
		<h1>Mi primera página jsp</h1>
		<%
		//esto indica que es java. a esto se le llama Scriptless
			int a = 20;
			int b = 5;
			out.print(" La suma de "+a+" y "+b+" es: ");
		%>
		<%= a+b //esto es equivalente a un echo%>
		<% if( a < 10){ %>
			suma fácil
		<%} else { %>
			la cosa se pone seria
			<%= a+b %>
		<%} %>
	</body>
</html>